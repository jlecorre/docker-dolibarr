# How to spawn a Dolibarr server using a Traefik proxy

## Requirements

* A Docker daemon
* A functional `docker-compose` [command line](https://docs.docker.com/compose/install/)
* A functional [MySQL](https://www.mysql.com/) or [MariaDB](https://mariadb.org/) server reachabled from a running Docker container

## Information

This project relies on the usage of the following repositories:

* [tuxgasy/docker-dolibarr](https://github.com/tuxgasy/docker-dolibarr) to build a `customized` Dolibarr Docker image
* [jlecorre/docker-traefik](https://gitlab.com/jlecorre/docker-traefik) to spawn and configure a Traefik proxy server

For more information related to the Dolibarr project: [Dolibarr on Github](https://github.com/Dolibarr/dolibarr/)

## Start the Dolibarr server

### Configure the `dolibarr.env` file

Edit and modify the `dolibarr.env` file available in the repository to feat your environment.  
With this setup, the Dolibarr component is configured to "automagically" connect to the local database exposed through a Unix socket by the server used to host the running Docker container.

Please don't forget to check the [jlecorre/docker-traefik](https://gitlab.com/jlecorre/docker-traefik) to properly configured the required Traefik labels.

### Configure the `docker-compose` file

Unless some specific changes, the `docker-compose.yml` doesn't need to be modified.

### Configure your domain name

Depending on your DNS provider, it's up to you to setup the domain name used to publicly expose the Dolibarr server.

### Build the required Docker image

```bash
docker-compose --env-file dolibarr.env -f docker-compose.yml build
```

### Start the Docker container

```bash
docker-compose --env-file dolibarr.env -f docker-compose up -d [--build] [--force-recreate]
```

### Get the Dolibarr logs

```bash
docker-compose --env-file dolibarr.env -f docker-compose logs -f
```

### Delete all resources related to this repository

```bash
docker-compose --env-file dolibarr.env -f docker-compose.yml down --remove-orphans
docker volume rm docker-dolibarr_dolibarr-docs
# and don't forget to delete the Dolibarr database previously created by your own means
```

## Useful information

- The Dolibarr server can take several minutes to be started for the first boot due to the database population script.
- This container exposes its data through the following volume: `docker-dolibarr_dolibarr-docs`. This is useful if you want to backup these data with an external script.  
- Don't forget to backup your database by your own means.
