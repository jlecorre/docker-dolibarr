#
## @brief Customized Dolibarr Docker image
## @author Joel LE CORRE <git@sublimigeek.fr>
#

# Based on an un-official Dolibarr image
# https://github.com/tuxgasy/docker-dolibarr
FROM tuxgasy/dolibarr:13.0.4-php7.4

# Environment variables
# https://github.com/Dolibarr/dolibarr/releases
ENV DOLIBARR_VERSION 13.0.4
ENV PHP_VERSION 7.4

# System update
RUN \
  apt-get update && \
  apt-get -y dist-upgrade

# Update docker entrypoint before to start container
# Here, we want to use a SOCKET connection to access to the database instead of the TCP protocol
RUN \
  sed -i 's/--protocol tcp/--protocol ${DOLI_DB_PROTOCOL}/' /usr/local/bin/docker-run.sh

# Update the php.ini configuration file to prevent the following error
# (HY000/2002): No such file or directory in /var/www/html/core/db/mysqli.class.php on line 205
# as we used the mysql socket (already mounted in the container), we need to properly configure the php runtime to use it
RUN \
  sed -i 's#mysqli.default_socket =#mysqli.default_socket = /var/run/mysqld/mysqld.sock#' /usr/local/etc/php/php.ini

# Image cleanup
RUN \
  apt-get -y autoremove && \
  apt-get -y autoclean && \
  apt-get -y clean && \
  rm -rf /var/lib/apt/lists/*

# Labels customization
LABEL dolibarr_version ${DOLIBARR_VERSION}
LABEL php_version ${PHP_VERSION}
LABEL maintainer "Joel LE CORRE <gitlab@sublimigeek.fr>"
