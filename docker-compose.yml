# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# @description
#   Execute a Dolibarr instance in a Docker container
#   Used a MariaDB instance through a socket file mounted in the container
# @sources
#   https://www.dolibarr.org/
#   https://wiki.dolibarr.org/index.php
# @dockerfile
#   Non-Official Dockerfile + custom modifications
#   https://hub.docker.com/r/tuxgasy/dolibarr
# @details
#   This docker-compose file does not provided a working database server
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

version: "3.8"
services:

  dolibarr-server:
    image: on-demand/dolibarr:13.0.4-php7.4
    build:
      context: .
      dockerfile: Dockerfile
    container_name: dolibarr-srv
    restart: unless-stopped
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=frontend"
      - "traefik.http.services.dolibarr-server.loadbalancer.server.port=${TRAEFIK_CONFIG_LOADBALANCER_PORT}"
      - "traefik.http.routers.dolibarr-server.rule=Host(`${TRAEFIK_CONFIG_DNS_EXPOSED}`)"
      - "traefik.http.routers.dolibarr-server.entrypoints=web,websecure"
      - "traefik.http.routers.dolibarr-server.tls=true"
      - "traefik.http.routers.dolibarr-server.tls.certresolver=${TRAEFIK_CONFIG_CERT_RESOLVER}"
    env_file:
      - dolibarr.env
    networks:
      - frontend
    volumes:
      # binding of MySQL/MariaDB socket in container
      - /var/run/mysqld/mysqld.sock:/run/mysqld/mysqld.sock:ro
      # required to backup data uploaded to Dolibarr with an external script
      - dolibarr-docs:/var/www/documents
      # required to upload and install new modules (must be saved with an external script)
      - docker-dolibarr_dolibarr-modules:/var/www/html/custom:rw

volumes:
  dolibarr-docs:
  # This volume is not operate by docker-compose
  # It has been manually created: `docker volume create docker-dolibarr_dolibarr-modules`
  docker-dolibarr_dolibarr-modules:
    external: true

networks:
  frontend:
    name: frontend
    external: true
